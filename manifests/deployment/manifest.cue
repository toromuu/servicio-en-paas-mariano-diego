package characters_deployment

import (
  k "kumori.systems/kumori/kmv"
  s "cc.pushpull/characters/service:characters"
)

#Manifest: k.#DeploymentManifest & {

  ref: {
    domain: "characters.app"
    name: "characters_cfg"
    version: [0,0,1]
  }

  description: {

    service: s.#Manifest

    configuration: {
      // Assign the values to the service configuration parameters
      parameter: {
        language: "en"
      }
      resource: {}
    }

    hsize: {
      frontend: {
        $_instances: 1
      }
      worker: {
        $_instances: 1
      }
      redis:{
        $_instances: 1
      }
      queue: {
        $_instances: 1
      }
    }

  }
}

// Exposed to be used by kumorictl tool (mandatory)
deployment: (k.#DoDeploy & {_params:manifest: #Manifest}).deployment
