package redis

import (
  k "kumori.systems/kumori/kmv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "characters.app"
    name: "redis"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        redis: { protocol: "tcp", port: 6379 }
      }
    }

    config: {
      parameter: {
        appconfig: {}
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      redis: {
        name: "redis"

        image: {
          hub: {
            name: ""
            secret: ""
          }
          tag: "dtororam/cc_pushpull-redis:0.0.16"
        }
        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: []
          env: {}
        }
      }
    }
  }
}
