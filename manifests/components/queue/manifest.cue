package queue

import (
  k "kumori.systems/kumori/kmv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "characters.app"
    name: "queue"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        queue: { protocol: "tcp", port: 4222 }
      }
    }

    config: {
      parameter: {
        appconfig: {}
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      queue: {
        name: "queue"

        image: {
          hub: {
            name: ""
            secret: ""
          }
          tag: "dtororam/cc_pushpull-queue:0.0.16"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: []
          env: {}
        }
      }

    }
  }
}
