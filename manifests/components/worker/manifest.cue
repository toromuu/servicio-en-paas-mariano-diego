package worker

import (
  k "kumori.systems/kumori/kmv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "characters.app"
    name: "worker"
    version: [0,0,1]
  }

  description: {

    srv: {
      client: {
        queue: { protocol: "tcp" }
        redis: { protocol: "tcp" }
      }

    }

    config: {
      parameter: {
        appconfig: {}
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      worker: {
        name: "worker"
        image: {
          hub: {
            name: ""
            secret: ""
          }
          tag: "dtororam/cc_pushpull-worker:0.0.16"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: []
          env: {
            
          }
        }
      }

    }
  }
}
