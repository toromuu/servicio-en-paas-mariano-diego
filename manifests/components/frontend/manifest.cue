package frontend

import (
  k "kumori.systems/kumori/kmv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "characters.app"
    name: "frontend"
    version: [0,0,2]
  }

  description: {

    srv: {
      client: {
        queue : { protocol: "tcp" }
        redis : { protocol: "tcp" }
      }
      server: {
        restapi : { protocol: "http", port: 8080 }
      }
    }

    config: {
      parameter: {
        appconfig: {}
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      frontend: {
        name: "frontend"

        image: {
          hub: {
            name: ""
            secret: ""
          }
          tag: "dtororam/cc_pushpull-frontend:0.0.16"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: []
          env: {

          }
        }
      }

    }
  }
}
