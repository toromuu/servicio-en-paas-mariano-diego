package characters

import (
  k "kumori.systems/kumori/kmv"
  d "cc.pushpull/characters/components/redis"
  q "cc.pushpull/characters/components/queue"
  f "cc.pushpull/characters/components/frontend"
  w "cc.pushpull/characters/components/worker"
)

#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "characters.app"
    name: "characters_service"
    version: [0,0,1]
  }

  description: {

    //
    // Kumori Component roles and configuration
    //

    // Configuration (parameters and resources) to be provided to the Kumori
    // Service Application.
    config: {
      parameter: {
        language: string
      }
      resource: {}
    }

    // List of Kumori Components of the Kumori Service Application.
    role: {
      frontend: k.#Role
      frontend: artifact: f.#Manifest

      worker: k.#Role
      worker: artifact: w.#Manifest

      redis: k.#Role
      redis: artifact: d.#Manifest

      queue: k.#Role
      queue: artifact: q.#Manifest
    }

    // Configuration spread:
    // Using the configuration service parameters, spread it into each role
    // parameters
    role: {


      redis: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }

      queue: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }

      frontend: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }

      worker: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }

    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    // Connectivity of a service application: the set of channels it exposes.
    srv: {
      server: {
        characters: { protocol: "http", port: 80 }
      }
    }

    // Connectors, providing specific patterns of communication among channels.
    connector: {
      inbound: { kind: "lb" }
      frontqueueconnector: { kind: "full" }
      workerqueueconnector: { kind: "full" }
      fdbconnector: { kind: "lb" }
      wdbconnector: { kind: "lb" }
    }

    // Links specify the topology graph.
    link: {

      // Outside -> FrontEnd (LB connector)
      self: characters: to: "inbound"
      inbound: to: frontend: "restapi"
      
      
      // Components

      
      frontend: queue: to: "frontqueueconnector"
      frontqueueconnector: to: queue: "queue"

      worker: queue: to: "workerqueueconnector"
      workerqueueconnector: to: queue : "queue"

    
      frontend: redis: to: "fdbconnector"
      fdbconnector: to: redis: "redis"

      worker: redis: to: "wdbconnector"
      wdbconnector: to: redis: "redis"
       

    }
  }
}
