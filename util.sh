#!/bin/bash

INBOUNDNAME="characters.examples/charactersinb"
DEPLOYNAME="characters.examples/charactersdep"

SERVICEURL="characters.vera.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard.vera.kumori.cloud"


case $1 in

'deploy-inbound')
  kumorictl register inbound $INBOUNDNAME \
    --domain $SERVICEURL \
    --cert $CLUSTERCERT
  ;;

'deploy-characters')
  kumorictl register deployment $DEPLOYNAME \
    --deployment ./manifests/deployment \
    --comment "Characters service"
  ;;

'link')
  kumorictl link $DEPLOYNAME:characters $INBOUNDNAME
  ;;

'test-POST')
  curl -X POST 'https://characters.vera.kumori.cloud/store/my-key\?character\=bilbo\&apellido\=bolson'
  ;;

'test-GET')
  curl -X GET 'https://characters.vera.kumori.cloud/my-key'
  ;;

  'describe')
  kumorictl describe deployment $DEPLOYNAME 
  ;;

  'exec-front')
  kumorictl exec --tty characters.examples/charactersdep frontend $2 frontend bash
  ;;

  'exec-redis')
  kumorictl exec --tty characters.examples/charactersdep redis $2 redis bash
  ;;


# Undeploy all (secrets, inbounds, deployments)
'undeploy-all')
  kumorictl unlink $DEPLOYNAME:characters $INBOUNDNAME
  kumorictl unregister deployment $DEPLOYNAME
  kumorictl unregister inbound $INBOUNDNAME
  ;;

esac
