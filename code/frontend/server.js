const express = require('express');
const { ColaDeTrabajos } = require( "./ColaDeTrabajos.js" )
const redisClient = require('./redis-client');
const app = express();


const NATS_URL = "0.queue:4222"


function getRandomString() {
	let fecha = ( Date.now() % 123456789 ).toString()
	let sufijo =  Math.random().toString(36).substr(2,8) 
	return fecha+sufijo
}

console.log("hola")

app.post('/store/:key', async (req, res) => {

  console.log("antes")

  var cdtClient = await new ColaDeTrabajos(
    NATS_URL,
    {worker: false, nombreCliente: "colaStore" }
  )

  console.log("despues")

  var workId = getRandomString()

  const id = await cdtClient.anyadirTrabajo(
    {
      idTrabajo: workId,
      funcion: "prueba",
      parametros: {key:req.params, value: req.query}
    })

  var workRes = await cdtClient.pedirRespuesta(id)

  console.log(workRes);

  return res.send('Success');
});


app.get('/:key', async (req, res) => {
  const { key } = req.params;
  const rawData = await redisClient.getAsync(key);
  return res.json(JSON.parse(rawData));
});



const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
