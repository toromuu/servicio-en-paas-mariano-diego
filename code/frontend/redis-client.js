const redis = require('redis');
const {promisify} = require('util');
//const client = redis.createClient({
//  host: '0.0.0.0',
//  port: 6379
//});

const client = redis.createClient({ url: 'redis://0.redis:80' });

module.exports = {
  ...client,
  getAsync: promisify(client.get).bind(client),
  setAsync: promisify(client.set).bind(client),
  keysAsync: promisify(client.keys).bind(client)
};
