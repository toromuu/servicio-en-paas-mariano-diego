const { esperar, ColaDeTrabajos } = require( "./ColaDeTrabajos.js" )
const redisClient = require('./redis-client');

const assert = require( "assert" )
const { AssertionError } = require("assert")


//const NATS_URL =  "0.0.0.0:" + process.env.TCP_NATS_PORT_ENV

const NATS_URL = "0.queue:4222"


async function main() {


	try {

		var cdtWorker = await new ColaDeTrabajos( NATS_URL, {worker: true} )

		assert( cdtWorker )
		console.log( " \n cdtWorker \n" )

        while(true)
        {
            try
            {
                const trabajo = await cdtWorker.pedirTrabajo()
                assert(trabajo)
                console.log( "trabajo recibido:\t " + trabajo.idTrabajo )
		        console.log( "trabajo recibido:\t " + trabajo.funcion )
		        console.log( "trabajo recibido:\t " + JSON.stringify( trabajo.parametros ) )
				

				valorRespuesta = await add(redisClient, trabajo.parametros)

				console.log(valorRespuesta);

				await cdtWorker.responderATrabajo(
					trabajo,
					{ estado: "OK", respuesta: valorRespuesta } 
				)

                console.log("Volviendo a pedir trabajo...")
            }

            catch (error)
            {
                if(error instanceof AssertionError)
                {
                    //console.log("No hay trabajo todavía...")
                    await esperar(5000)
                }
                else
                {
                    await cdtWorker.responderATrabajo(
						trabajo,
						{ estado: "NOK", respuesta: { errorResponse : error } } // respuestas
					)
					throw error
                }
            }
        }

		// return // para depurar por pasos
		
		// .................................................................
		// .................................................................
	} catch( error ) {
		console.log( "\n\nworker.js CATCH: " )
		console.log( error )

		await cdtWorker.cerrar()
	}

} // main()

async function add(redisClient, req) {
	let conn;
	try {

	  const { key } = req.key;
	  const value = req.value;
	  await redisClient.setAsync(key, JSON.stringify(value));
	  console.log("Inserccion")
	  return "OK";
  
	} catch (err) {
	  throw err;
	} finally {
	  if (conn) return conn.end();
	}
  }

// ---------------------------------------------------------------------
main()
// ---------------------------------------------------------------------

